package com.example.chesstimer.data.repository

import androidx.lifecycle.LiveData
import com.example.chesstimer.data.ResultDao
import com.example.chesstimer.model.Result

class ResultRepositoryImpl(val resultDao:ResultDao):ResultRepository {
    override fun save(result:Result) = resultDao.save(result)
    override fun delete(result:Result) = resultDao.delete(result)
    override fun getResultById(id: Long): Result? = resultDao.getResultById(id)
    override fun getAllResults(): LiveData<List<Result>> = resultDao.getAllResults()
}