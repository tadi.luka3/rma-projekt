package com.example.chesstimer.ui.game_fragment

import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.chesstimer.R
import com.example.chesstimer.databinding.FragmentGameBinding
import com.example.chesstimer.presentation.GameViewModel
import com.example.chesstimer.ui.start_fragment.StartFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class GameFragment: Fragment() {
    private lateinit var binding : FragmentGameBinding
    private val viewModel:GameViewModel by viewModel()
    private val args by navArgs<GameFragmentArgs>()
    private lateinit var mSoundPool: SoundPool
    private var mLoaded: Boolean = false
    var mSoundMap: HashMap<Int, Int> = HashMap()
    private var START_TIME_IN_MILLIS1: Long = 0
    private var START_TIME_IN_MILLIS2: Long = 0
    private var mTimeLeftInMillis1: Long = 0
    private var mTimeLeftInMillis2: Long = 0
    private var mPaused1 = false
    private var mPaused2 = false
    private var mCountDownTimer1: CountDownTimer? = null
    private var mCountDownTimer2: CountDownTimer? = null
    private var mTimerRunning1 = false
    private var mTimerRunning2 = false
    private var movesWhite:Int=0
    private var movesBlack:Int=0
    private var time:Long=0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGameBinding.inflate(layoutInflater)
        val nameWhite:String=args.stringWhite
        val nameBlack:String=args.stringBlack
        time=args.longTime
        binding.vrijemeBijeli.isEnabled=false
        binding.vrijemeCrni.isEnabled=false
        START_TIME_IN_MILLIS1 = time
        START_TIME_IN_MILLIS2 = time
        mTimeLeftInMillis1 = time
        mTimeLeftInMillis2 = time
        updateCountDownText1()
        updateCountDownText2()
        binding.imeBijeli.text=nameWhite
        binding.imeCrni.text=nameBlack
        binding.pause.visibility=View.INVISIBLE
        binding.pause.isEnabled=false
        binding.pobjedaBijeli.isEnabled=false
        binding.pobjedaCrni.isEnabled=false
        loadSounds()

        binding.play.setOnClickListener {
            binding.pause.visibility=View.VISIBLE
            binding.play.visibility=View.INVISIBLE
            binding.play.isEnabled=false
            binding.pause.isEnabled=true
            if (mPaused1) {
                startTimer1()
                binding.vrijemeBijeli.isEnabled=true
                binding.pobjedaBijeli.isEnabled=true
                mPaused1 = false
            } else if (mPaused2) {
                startTimer2()
                binding.vrijemeCrni.isEnabled=true
                binding.pobjedaCrni.isEnabled=true
                mPaused2 = false
            } else {
                startTimer1()
                binding.vrijemeBijeli.isEnabled=true
                binding.pobjedaBijeli.isEnabled=true
            }
        }

        binding.pause.setOnClickListener{
            binding.play.visibility=View.VISIBLE
            binding.play.isEnabled=true
            binding.pause.isEnabled=false
            binding.pause.visibility=View.INVISIBLE
            if (mTimerRunning1) {
                pauseTimer1()
                mPaused1 = true
                binding.vrijemeBijeli.isEnabled=false
                binding.vrijemeCrni.isEnabled=false
                binding.pobjedaBijeli.isEnabled=false
                binding.pobjedaCrni.isEnabled=false
            }
            if (mTimerRunning2) {
                pauseTimer2()
                mPaused2 = true
                binding.pobjedaBijeli.isEnabled=false
                binding.pobjedaCrni.isEnabled=false
                binding.vrijemeBijeli.isEnabled=false
                binding.vrijemeCrni.isEnabled=false
            }
        }


    binding.vrijemeBijeli.setOnClickListener{
        playSound(R.raw.clock)
        movesWhite++
        if (mTimerRunning1) {
            pauseTimer1()
            startTimer2()
            binding.vrijemeBijeli.isEnabled=false
            binding.vrijemeCrni.isEnabled=true
            binding.pobjedaBijeli.isEnabled=false
            binding.pobjedaCrni.isEnabled=true

        } else {
            startTimer2()
            binding.vrijemeBijeli.isEnabled=false
            binding.vrijemeCrni.isEnabled=true
            binding.pobjedaBijeli.isEnabled=false
            binding.pobjedaCrni.isEnabled=true

        }
    }

        binding.vrijemeCrni.setOnClickListener{
            playSound(R.raw.clock)
            movesBlack++
            if (mTimerRunning2) {
                pauseTimer2()
                startTimer1()
                binding.vrijemeCrni.isEnabled=false
                binding.vrijemeBijeli.isEnabled=true
                binding.pobjedaBijeli.isEnabled=true
                binding.pobjedaCrni.isEnabled=false

            } else {
                startTimer1()
                binding.vrijemeCrni.isEnabled=false
                binding.vrijemeBijeli.isEnabled=true
                binding.pobjedaBijeli.isEnabled=true
                binding.pobjedaCrni.isEnabled=false
            }
        }

            binding.pobjedaCrni.setOnClickListener {
                playSound(R.raw.victory)
                movesBlack++
                val currentDate = SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault()).format(
                    Date()
                )
                val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(
                    Date()
                )
                val date: String = currentDate + " " + currentTime
                val minutes1 = ((time - mTimeLeftInMillis1) / 1000).toInt() / 60
                val seconds1 = ((time - mTimeLeftInMillis1) / 1000).toInt() % 60
                val timeLeftFormatted1 =
                    String.format(Locale.getDefault(), "%02d:%02d", minutes1, seconds1)
                val minutes2 = ((time - mTimeLeftInMillis2) / 1000).toInt() / 60
                val seconds2 = ((time - mTimeLeftInMillis2) / 1000).toInt() % 60
                val timeLeftFormatted2 =
                    String.format(Locale.getDefault(), "%02d:%02d", minutes2, seconds2)
                val gameLength = (time / 60000).toString() + " min"
                val winner: String = binding.imeCrni.text.toString()
                val loser: String = binding.imeBijeli.text.toString()
                viewModel.save(
                    winner,
                    loser,
                    movesBlack,
                    movesWhite,
                    timeLeftFormatted2,
                    timeLeftFormatted1,
                    gameLength,
                    date
                )

                val action = GameFragmentDirections.actionGameFragmentToListFragment()
                findNavController().navigate(action)



        }

        binding.pobjedaBijeli.setOnClickListener {
            playSound(R.raw.victory)
            movesWhite++
            val currentDate = SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault()).format(Date())
            val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
            val date: String = currentDate + " " + currentTime
            val minutes1 = ((time - mTimeLeftInMillis1) / 1000).toInt() / 60
            val seconds1 = ((time - mTimeLeftInMillis1) / 1000).toInt() % 60
            val timeLeftFormatted1 =
                String.format(Locale.getDefault(), "%02d:%02d", minutes1, seconds1)
            val minutes2 = ((time - mTimeLeftInMillis2) / 1000).toInt() / 60
            val seconds2 = ((time - mTimeLeftInMillis2) / 1000).toInt() % 60
            val timeLeftFormatted2 =
                String.format(Locale.getDefault(), "%02d:%02d", minutes2, seconds2)
            val gameLength = (time / 60000).toString() + " min"
            val winner: String = binding.imeBijeli.text.toString()
            val loser: String = binding.imeCrni.text.toString()
            viewModel.save(
                winner,
                loser,
                movesWhite,
                movesBlack,
                timeLeftFormatted1,
                timeLeftFormatted2,
                gameLength,
                date
            )

            val action = GameFragmentDirections.actionGameFragmentToListFragment()
            findNavController().navigate(action)


        }
        binding.resetButton.setOnClickListener{
            val action=GameFragmentDirections.actionGameFragmentToStartFragment()
            findNavController().navigate(action)
        }

        return binding.root
    }

    private fun pauseTimer2() {
        mCountDownTimer2!!.cancel()
        mTimerRunning2 = false
    }

    private fun pauseTimer1() {
        mCountDownTimer1!!.cancel()
        mTimerRunning1 = false
    }

    private fun startTimer2() {
        mCountDownTimer2 = object : CountDownTimer(mTimeLeftInMillis2, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis2 = millisUntilFinished
                updateCountDownText2()
            }

            override fun onFinish() {
                mTimerRunning2 = false
                val currentDate = SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault()).format(Date())
                val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
                val date:String= currentDate + " "+ currentTime
                val minutes1 = ((time - mTimeLeftInMillis1) / 1000).toInt() / 60
                val seconds1 = ((time-mTimeLeftInMillis1) / 1000).toInt() % 60
                val timeLeftFormatted1 = String.format(Locale.getDefault(), "%02d:%02d", minutes1, seconds1)
                val minutes2 = ((time - mTimeLeftInMillis2) / 1000).toInt() / 60
                val seconds2 = ((time-mTimeLeftInMillis2) / 1000).toInt() % 60
                val timeLeftFormatted2 = String.format(Locale.getDefault(), "%02d:%02d", minutes2, seconds2)
                val gameLength = (time/60000).toString() + " min"
                val winner:String=binding.imeBijeli.text.toString()
                val loser:String=binding.imeCrni.text.toString()
                viewModel.save(winner,loser,movesWhite,movesBlack,timeLeftFormatted1,timeLeftFormatted2,gameLength,date)

                val action=GameFragmentDirections.actionGameFragmentToListFragment()
                findNavController().navigate(action)
            }
        }.start()
        mTimerRunning2 = true
    }

    private fun startTimer1() {
        mCountDownTimer1 = object:CountDownTimer(mTimeLeftInMillis1, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTimeLeftInMillis1 = millisUntilFinished
                updateCountDownText1()
            }

            override fun onFinish() {
                mTimerRunning1 = false
                val currentDate = SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault()).format(Date())
                val currentTime = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
                val date:String= currentDate + " "+ currentTime
                val minutes1 = ((time - mTimeLeftInMillis1) / 1000).toInt() / 60
                val seconds1 = ((time-mTimeLeftInMillis1) / 1000).toInt() % 60
                val timeLeftFormatted1 = String.format(Locale.getDefault(), "%02d:%02d", minutes1, seconds1)
                val minutes2 = ((time - mTimeLeftInMillis2) / 1000).toInt() / 60
                val seconds2 = ((time-mTimeLeftInMillis2) / 1000).toInt() % 60
                val timeLeftFormatted2 = String.format(Locale.getDefault(), "%02d:%02d", minutes2, seconds2)
                val gameLength = (time/60000).toString() + " min"
                val winner:String=binding.imeCrni.text.toString()
                val loser:String=binding.imeBijeli.text.toString()
                viewModel.save(winner,loser,movesBlack,movesWhite,timeLeftFormatted2,timeLeftFormatted1,gameLength,date)

                val action=GameFragmentDirections.actionGameFragmentToListFragment()
                findNavController().navigate(action)
            }
        }.start()
        mTimerRunning1=true
    }

    private fun updateCountDownText2() {
        val minutes = (mTimeLeftInMillis2 / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis2 / 1000).toInt() % 60
        val timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        binding.vrijemeCrni.text=timeLeftFormatted
    }

    private fun updateCountDownText1() {
        val minutes = (mTimeLeftInMillis1 / 1000).toInt() / 60
        val seconds = (mTimeLeftInMillis1 / 1000).toInt() % 60
        val timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        binding.vrijemeBijeli.text=timeLeftFormatted
    }
    companion object {
        val Tag = "game"

        fun create(): Fragment {
            return GameFragment()
        }
    }

    private fun playSound(selectedSound: Int) {
        val soundID = this.mSoundMap[selectedSound] ?: 0
        this.mSoundPool.play(soundID, 1f, 4f, 1, 0, 1f)
    }

    private fun loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = SoundPool.Builder().setMaxStreams(10).build()
        } else {
            this.mSoundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        }
        this.mSoundPool.setOnLoadCompleteListener { _, _, _ -> mLoaded = true }
        this.mSoundMap[R.raw.clock] = this.mSoundPool.load(this.activity, R.raw.clock, 1)
        this.mSoundMap[R.raw.victory] = this.mSoundPool.load(this.activity, R.raw.victory, 1)

    }
}


