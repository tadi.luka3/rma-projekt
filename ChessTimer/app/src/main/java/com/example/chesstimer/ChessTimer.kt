package com.example.chesstimer

import android.app.Application
import com.example.chesstimer.di.databaseModule
import com.example.chesstimer.di.preferenceModule
import com.example.chesstimer.di.repositoryModule
import com.example.chesstimer.di.viewmodelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level


class ChessTimer:Application() {

    override fun onCreate() {
        super.onCreate()
        application = this


        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@ChessTimer)
            modules(
                databaseModule,
                repositoryModule,
                preferenceModule,
                viewmodelModule

            )

        }
    }


    companion object{
        lateinit var application: Application
    }
}