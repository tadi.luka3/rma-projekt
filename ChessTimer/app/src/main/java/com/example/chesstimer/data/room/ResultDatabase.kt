package com.example.chesstimer.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.chesstimer.data.ResultDao
import com.example.chesstimer.model.Result


@Database(
    entities = [Result::class],
    version = 1,
    exportSchema = false
)
abstract class ResultDatabase:RoomDatabase() {
    abstract fun getTaskDao(): ResultDao


    companion object {

        private const val databaseName = "resultDb"

        @Volatile
        private var INSTANCE: ResultDatabase? = null

        fun getDatabase(context: Context): ResultDatabase {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = buildDatabase(context)
                }
            }
            return INSTANCE!!
        }
        private fun buildDatabase(context: Context): ResultDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                ResultDatabase::class.java,
                databaseName
            )
                .allowMainThreadQueries()
                .build()
        }
    }

}