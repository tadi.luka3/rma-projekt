package com.example.chesstimer.ui.list_fragment

import com.example.chesstimer.model.Result

interface OnResultEventListener {
    fun onResultSelected(id: Long?)
    fun onResultLongPress(result: Result?): Boolean
}