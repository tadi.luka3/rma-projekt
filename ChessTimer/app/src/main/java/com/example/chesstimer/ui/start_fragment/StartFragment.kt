package com.example.chesstimer.ui.start_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.chesstimer.databinding.FragmentStartBinding
import com.example.chesstimer.presentation.StartViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class StartFragment: Fragment() {

    private lateinit var binding: FragmentStartBinding
    private val viewModel:StartViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartBinding.inflate(layoutInflater)
        binding.rbn1.isChecked=true
        viewModel.setName1(binding.whiteEt.text.toString())
        viewModel.setName2(binding.blackEt.text.toString())
        if (binding.rbn1.isChecked)  viewModel.setTime(180000)
        if (binding.rbn2.isChecked) viewModel.setTime(300000)
        if (binding.rbn3.isChecked) viewModel.setTime(600000)
        if (binding.rbn4.isChecked) viewModel.setTime(1800000)

        viewModel.name1.observe(viewLifecycleOwner, {name1:String -> updateName1(name1)})
        viewModel.name2.observe(viewLifecycleOwner, {name2:String -> updateName2(name2)})
        viewModel.time.observe(viewLifecycleOwner, {time:Long -> displayRbn(time)})
        binding.btnPlay.setOnClickListener { startGame() }
        binding.btnResults.setOnClickListener{goToResults()}
        return binding.root
    }

    private fun goToResults() {
        val action=StartFragmentDirections.actionStartFragmentToListFragment()
        findNavController().navigate(action)
    }

    private fun updateName2(name2: String) {
        binding.blackEt.setText(name2)
        viewModel.saveName2()
    }

    private fun updateName1(name1: String) {
        binding.whiteEt.setText(name1)
        viewModel.saveName1()
    }

    private fun displayRbn(time: Long) {
        if(time.equals(180000))binding.rbn1.isChecked=true
        if(time.equals(300000))binding.rbn2.isChecked=true
        if(time.equals(600000))binding.rbn3.isChecked=true
        if(time.equals(1800000))binding.rbn4.isChecked=true
        viewModel.saveTime()

    }


    private fun startGame() {
        val nameWhite=binding.whiteEt.text.toString()
        val nameBlack=binding.blackEt.text.toString()
        if (nameWhite.isEmpty() || nameBlack.isEmpty()) {
            makeToast()
        }
        var time:Long = 180000;

        if (binding.rbn1.isChecked) time = 180000
        if (binding.rbn2.isChecked) time = 300000
        if (binding.rbn3.isChecked) time = 600000
        if (binding.rbn4.isChecked) time = 1800000


        if(nameWhite.isNotEmpty() && nameBlack.isNotEmpty()) {


            val action = StartFragmentDirections.actionStartFragmentToGameFragment(
                nameWhite,
                nameBlack,
                time
            )
            findNavController().navigate(action)
        }
    }

    private fun makeToast() {
        Toast.makeText(activity, "Unesite ime igrača molim!", Toast.LENGTH_SHORT).show()
    }

    companion object {
        val Tag = "NewGame"

        fun create(): Fragment {
            return StartFragment()
        }
    }
}