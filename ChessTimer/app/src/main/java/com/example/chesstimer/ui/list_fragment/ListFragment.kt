package com.example.chesstimer.ui.list_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chesstimer.databinding.FragmentListBinding
import com.example.chesstimer.model.Result
import com.example.chesstimer.presentation.ListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment:Fragment(), OnResultEventListener {
    private lateinit var binding: FragmentListBinding
    private lateinit var adapter: ResultAdapter
    private val viewModel: ListViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(layoutInflater)
        binding.fabGoBack.setOnClickListener { returnToStart() }
        setupRecyclerView()
        viewModel.results.observe(viewLifecycleOwner) {
            if (it != null && it.isNotEmpty()) {
                adapter.setResults(it)
            }
        }
        return binding.root
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(
            context,
            LinearLayoutManager.VERTICAL,
            false
        )
        adapter = ResultAdapter()
        adapter.onResultSelectedListener = this
        binding.recyclerView.adapter = adapter
    }
    companion object {
        val Tag = "ResultsList"

        fun create(): Fragment {
            return ListFragment()
        }
    }

    private fun returnToStart() {
        val action=ListFragmentDirections.actionListFragmentToStartFragment()
        findNavController().navigate(action)
    }

    override fun onResultSelected(id: Long?) {
        val action = ListFragmentDirections.actionListFragmentToDetailsFragment(id ?: -1)
        findNavController().navigate(action)
    }

    override fun onResultLongPress(result: Result?): Boolean {
        result?.let { it ->
            viewModel.delete(it)
        }
        return true
    }
}