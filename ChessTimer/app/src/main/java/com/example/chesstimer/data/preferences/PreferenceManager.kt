package com.example.chesstimer.data.preferences

import android.content.Context

class PreferenceManager(private val context: Context) {


    companion object {
        const val PREFS_FILE = "MyPreferences"
        const val key1 = "WhitePlayer"
        const val key2 = "BlackPlayer"
        const val key3 = "time"
        const val key4 = "TimeWhite"
        const val key5 = "TimeBlack"
    }
    fun saveName1(name1: String) {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        val editor = sharedPreferences.edit()
        editor.putString(key1, name1)

        editor.apply()
    }
    fun saveName2(name2: String) {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        val editor = sharedPreferences.edit()
        editor.putString(key2, name2)

        editor.apply()
    }
    fun saveTime(time:Long) {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        val editor = sharedPreferences.edit()
        editor.putLong(key3,time)

        editor.apply()
    }
    fun retrieveName1(): String? {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        return sharedPreferences.getString(key1,"luka")
    }
    fun retrieveName2(): String? {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        return sharedPreferences.getString(key2,"ivan")
    }
    fun retrieveTime(): Long {
        val sharedPreferences = context.getSharedPreferences(
            PREFS_FILE, Context.MODE_PRIVATE
        )
        return sharedPreferences.getLong(key3,0)
    }
}