package com.example.chesstimer.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.chesstimer.data.preferences.PreferenceManager

class StartViewModel(private val preferenceManager: PreferenceManager):ViewModel(){
    private val _name1 = MutableLiveData<String>(preferenceManager.retrieveName1())
    val name1: LiveData<String> = _name1

    private val _name2 = MutableLiveData<String>(preferenceManager.retrieveName2())
    val name2: LiveData<String> = _name2

    private val _time = MutableLiveData<Long>(preferenceManager.retrieveTime())
    val time: LiveData<Long> = _time

    fun setName1(name:String){
        _name1.value=name
    }
    fun setName2(name:String){
        _name2.value=name
    }

    fun setTime(time:Long){
        _time.value=time
    }


    fun saveName1(){
        preferenceManager.saveName1(name1.value ?: "luka")
    }
    fun saveName2(){
        preferenceManager.saveName2(name2.value ?: "ivan")
    }
    fun saveTime(){
        preferenceManager.saveTime(time.value ?: 180000)
    }

}
