package com.example.chesstimer.ui.details_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.chesstimer.databinding.FragmentDetailsBinding
import com.example.chesstimer.model.Result
import com.example.chesstimer.presentation.DetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment:Fragment() {

    private lateinit var binding: FragmentDetailsBinding
    private val viewModel: DetailsViewModel by viewModel()
    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(layoutInflater)
        binding.btnBack.setOnClickListener{goBackToList()}
        return binding.root
    }

    private fun goBackToList() {
        val action=DetailsFragmentDirections.actionDetailsFragmentToListFragment()
        findNavController().navigate(action)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val result = viewModel.getTaskById(args.resultId)
        display(result)
    }

    private fun display(result: Result?) {
        result?.let {
            binding.apply {
                txtPobjednik.text=result.pobjednik
                txtGubitnik.text=result.gubitnik
                txtPotezi1.text=result.potezi_w.toString()
                txtPotezi2.text=result.potezi_g.toString()
                txtDatum.text=result.dateTime
                txtTrajanje.text=result.trajanje
                protekloPobjednik.text=result.protekloPobjednik
                protekloGubitnik.text=result.protekloGubitnik
                return
            }
        }
    }
    companion object {
        val Tag = "Details"

        fun create(): Fragment {
            return DetailsFragment()
        }
    }




}