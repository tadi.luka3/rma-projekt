package com.example.chesstimer.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.chesstimer.model.Result


@Dao
interface ResultDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(result: Result)

    @Delete
    fun delete(result: Result)

    @Query("SELECT * FROM results WHERE id =:id ")
    fun getResultById(id: Long): Result?

    @Query("SELECT * FROM results ORDER BY dateTime DESC")
    fun getAllResults(): LiveData<List<Result>>


}