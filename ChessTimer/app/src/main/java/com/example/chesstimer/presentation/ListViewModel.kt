package com.example.chesstimer.presentation

import androidx.lifecycle.ViewModel
import com.example.chesstimer.data.repository.ResultRepository
import com.example.chesstimer.model.Result

class ListViewModel(
    val resultRepository: ResultRepository
):ViewModel() {
    val results = resultRepository.getAllResults()

    fun delete(result: Result) {
        resultRepository.delete(result)
    }
}