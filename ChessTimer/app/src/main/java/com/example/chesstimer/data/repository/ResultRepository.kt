package com.example.chesstimer.data.repository

import androidx.lifecycle.LiveData
import com.example.chesstimer.model.Result

interface ResultRepository {
    fun save(result: Result)
    fun delete(result: Result)
    fun getResultById(id: Long): Result?
    fun getAllResults(): LiveData<List<Result>>
}