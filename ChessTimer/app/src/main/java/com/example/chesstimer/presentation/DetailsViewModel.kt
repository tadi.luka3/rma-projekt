package com.example.chesstimer.presentation

import androidx.lifecycle.ViewModel
import com.example.chesstimer.data.repository.ResultRepository
import com.example.chesstimer.model.Result

class DetailsViewModel(
    val resultRepository: ResultRepository
):ViewModel() {

    fun getTaskById(id: Long?):  Result?{
        var result: Result? = null
        id?.let { result = resultRepository.getResultById(id) }
        return result
    }
}