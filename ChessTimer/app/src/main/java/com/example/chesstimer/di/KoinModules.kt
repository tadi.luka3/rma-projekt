package com.example.chesstimer.di

import android.app.Application
import com.example.chesstimer.data.ResultDao
import com.example.chesstimer.data.preferences.PreferenceManager
import com.example.chesstimer.data.repository.ResultRepository
import com.example.chesstimer.data.repository.ResultRepositoryImpl
import com.example.chesstimer.data.room.ResultDatabase
import com.example.chesstimer.presentation.DetailsViewModel
import com.example.chesstimer.presentation.GameViewModel
import com.example.chesstimer.presentation.ListViewModel
import com.example.chesstimer.presentation.StartViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val databaseModule = module {
    fun provideDatabase(application: Application): ResultDatabase {
        return ResultDatabase.getDatabase(application)
    }
    fun provideTaskDao(database: ResultDatabase): ResultDao{
        return database.getTaskDao()
    }
    single<ResultDatabase> { provideDatabase(get()) }
    single<ResultDao> { provideTaskDao(get()) }
}

val repositoryModule = module {
    single<ResultRepository> { ResultRepositoryImpl(get()) }
}
val preferenceModule = module {
    single<PreferenceManager> { PreferenceManager(get()) }
}
val viewmodelModule = module {
    viewModel { StartViewModel(get())  }
    viewModel { GameViewModel(get())  }
    viewModel { ListViewModel(get())  }
    viewModel { DetailsViewModel(get())  }
}