package com.example.chesstimer.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "results")
data class Result(
    @PrimaryKey(autoGenerate = true)
     var id: Long=0,

    @ColumnInfo(name = "winner")
    val pobjednik: String,

    @ColumnInfo(name = "loser")
     val gubitnik: String,

    @ColumnInfo(name = "dateTime")
     val dateTime: String,

    @ColumnInfo(name = "potezi_w")
     val potezi_w: Int,

    @ColumnInfo(name = "potezi_g")
     val potezi_g: Int,

    @ColumnInfo(name = "trajanje")
     val trajanje: String,

    @ColumnInfo(name = "protekloPobjednik")
        val protekloPobjednik: String,

    @ColumnInfo(name = "protekloGubitnik")
    val protekloGubitnik: String
    ){

}
