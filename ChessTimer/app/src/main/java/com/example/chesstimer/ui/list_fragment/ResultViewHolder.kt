package com.example.chesstimer.ui.list_fragment

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.chesstimer.databinding.ItemResultBinding
import com.example.chesstimer.model.Result

class ResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

    fun bind(result:Result){
        val binding=ItemResultBinding.bind(itemView)
        binding.winnerLoser.text=result.pobjednik+ " je pobjedio "+result.gubitnik
        binding.datum.text=result.dateTime

    }
}