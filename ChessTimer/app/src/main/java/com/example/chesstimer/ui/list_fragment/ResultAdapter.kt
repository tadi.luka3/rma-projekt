package com.example.chesstimer.ui.list_fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.chesstimer.R
import com.example.chesstimer.model.Result

class ResultAdapter : RecyclerView.Adapter<ResultViewHolder>() {

    private val results = mutableListOf<Result>()
    var onResultSelectedListener: OnResultEventListener? = null

    fun setResults(results: List<Result>) {
        this.results.clear()
        this.results.addAll(results)
        this.notifyDataSetChanged()
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_result, parent, false)
        return ResultViewHolder(view)
    }

    override fun onBindViewHolder(holder: ResultViewHolder, position: Int) {
        val result = results[position]
        holder.bind(result)
        onResultSelectedListener?.let { listener ->
            holder.itemView.setOnClickListener { listener.onResultSelected(result.id) }
            holder.itemView.setOnLongClickListener { listener.onResultLongPress(result) }
        }
    }

    override fun getItemCount(): Int = results.count()



}