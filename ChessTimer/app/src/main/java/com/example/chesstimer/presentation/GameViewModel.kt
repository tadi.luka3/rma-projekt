package com.example.chesstimer.presentation

import androidx.lifecycle.ViewModel
import com.example.chesstimer.data.repository.ResultRepository
import com.example.chesstimer.model.Result

class GameViewModel(
    val resultRepository:ResultRepository
):ViewModel() {

    fun save (winner:String, loser:String, movesWinner:Int, movesLoser:Int,
              timeWinner:String, timeLoser:String, gameLength:String, date:String){

        resultRepository.save(Result(0,winner, loser, date, movesWinner, movesLoser,gameLength, timeWinner, timeLoser))

    }

}